#!/bin/bash

# This script installs k-maji-orquesta on cloud based system.
# Run this script from outside the project

## Project constants ##
# Set these vars according to your config, set disabled for not using
ENV_PREFIX="ko-"
SSH_TMP_USER=sysadmin
DEPLOYER=172.16.0.77
TESTER=172.16.0.78
CMDB=disabled
MONITOR=disabled
POSTGRES=disabled
CMS=172.16.0.76
DNS_PUBLIC=kronops.com.mx
DNS_PRIVATE=kronops.com.mx
CMDB_SAMPLE_DATA=no
BRANCH=jmedina/cms_focal
CODE_NAME='k-maji-orquesta'
SLAVE_LABEL="tester"
proxy_host=""
proxy_port=""
proxy_login=""
proxy_pass=""
DOCKER_USER=""
DOCKER_REPO=""
DOCKER_PWD=""
# End constants #

PROJECT=$1
ENV=$2
PROJECT_ROOT=/opt/$CODE_NAME
REPO_URL=https://gitlab.com/kronops/$CODE_NAME.git
INVENTORY=ansible/inventory/$PROJECT
ANSIBLE_VARS_FILE=$PROJECT_ROOT/ansible/group_vars/all
if [[ ! -z "$proxy_login" ]]; then
  PROXY=http://$proxy_login:$proxy_pass@$proxy_host:$proxy_port
else
  PROXY=http://$proxy_host:$proxy_port
fi

if which tput >/dev/null 2>&1; then
   ncolors=$(tput colors)
fi
if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
  RED="$(tput setaf 1)"
  GREEN="$(tput setaf 2)"
  YELLOW="$(tput setaf 3)"
  BLUE="$(tput setaf 4)"
  BOLD="$(tput bold)"
  NORMAL="$(tput sgr0)"
else
  RED=""
  GREEN=""
  YELLOW=""
  BLUE=""
  BOLD=""
  NORMAL=""
fi

checkSystem() {
  printf "${GREEN}==> Checking for proxy settings${NORMAL}\n"
  sudo env | grep proxy >/dev/null 2>&1
  if [[ "$?" == 1 ]]; then
    printf "${YELLOW}"
    read -p "==> No proxy settings, continue(y/n)? " -n 1 -r
    printf "${NORMAL}\n"
    [[ ! $REPLY =~ ^[Yy]$ ]] && exit 1
  else
    printf "${YELLOW}==> System with proxy configuration${NORMAL}\n"
  fi
  printf "${GREEN}==> Checking for dependencies${NORMAL}\n"
  command -v make > /dev/null 2>&1
  [[ "$?" == 1 ]] && sudo apt-get install -qq -y make
  command -v git > /dev/null 2>&1
  [[ "$?" == 1 ]] && sudo apt-get install -qq -y git
}

usage() {
  printf "${YELLOW}Run this script from outside the project\n"
  printf "Usage: ./$0 ${BOLD}'PROJECT' 'ENV'${NORMAL}\n"
  exit 1
}

clone() {
  printf "${GREEN}==> Cloning repo from $REPO_URL${NORMAL}\n"
  if [[ -d "$PROJECT_ROOT/" ]]; then
    printf "${GREEN}==> Backup previous installed version${NORMAL}\n"
    PROJECT_BAK=/opt/$CODE_NAME-$(date +%Y_%m_%d_%S)
    mv $PROJECT_ROOT $PROJECT_BAK
  fi
  cd /opt
  git clone -b $BRANCH --single-branch $REPO_URL
  cd $PROJECT_ROOT
}

setEnv(){
  printf "${GREEN}==> Set .env file ${NORMAL}${BOLD}$PROJECT $ENV${NORMAL}\n"
  touch $PROJECT_ROOT/.env
  cat << EOT > $PROJECT_ROOT/.env
CMS=$CMS
REPO_URL=$REPO_URL
PROJECT_ROOT=$PROJECT_ROOT
PROJECT_NAME=$PROJECT
PROJECT_ENV=$ENV
PROJECT_DOMAIN=$DNS_PUBLIC
SSH_TMP_USER=$SSH_TMP_USER
BRANCH=$BRANCH
CMDB_SAMPLE_DATA=$CMDB_SAMPLE_DATA
SLAVE_LABEL=$SLAVE_LABEL
DOCKER_USER=$DOCKER_USER
DOCKER_REPO=$DOCKER_SERVER
DOCKER_PWD=/var/lib/jenkins/.docker/auth
export PROJECT_ROOT
export CMDB_SAMPLE_DATA
export PROJECT_NAME
export PROJECT_ENV
export SSH_TMP_USER
export BRANCH
export SLAVE_LABEL
export REPO_URL
export DOCKER_USER
export DOCKER_REPO
export DOCKER_PWD
export CMS
EOT
if [[ ! -z "$proxy_host" ]]; then
cat << EOT >> $PROJECT_ROOT/.env
http_proxy=$PROXY
https_proxy=$PROXY
export http_proxy
export https_proxy
EOT
fi
}

checkEnvFile() {
  if [[ ! -f "$PROJECT_ROOT/.env" ]]; then
    printf "${RED}\n[WARNING]: File $PROJECT_ROOT/.env does not exist!!${NORMAL}\n"
    exit -1
  fi
}

createInventory(){
  printf "${GREEN}==> Create $INVENTORY/$ENV${NORMAL}\n"
  mkdir -p $PROJECT_ROOT/$INVENTORY
  touch $PROJECT_ROOT/$INVENTORY/$ENV
cat << EOT > $PROJECT_ROOT/$INVENTORY/$ENV
[all:vars]
project_name=$CODE_NAME
project_root=$PROJECT_ROOT
authorized_keys_for_central_servers=/etc/ansible/inventory/$PROJECT/.ssh/id-$PROJECT-ansible.rsa.pub
ansible_ssh_private_key_file=/etc/ansible/inventory/$PROJECT/.ssh/id-$PROJECT-ansible.rsa
useradmin_name=$SSH_TMP_USER
dns_public_domain=$DNS_PUBLIC
dns_private_domain=$DNS_PRIVATE
timezone=America/Mexico_City
jenkins_url=http://$DEPLOYER:8080
itop_ip=$CMDB
deployer_ip=$DEPLOYER

[central:children]
central_deployer
central_cms
central_tester
central_cmdb
central_postgres
central_monitor
#central_hardening

[central_deployer]
${ENV_PREFIX}deployer.$DNS_PUBLIC  ansible_ssh_host=$DEPLOYER

[central_cms]
${ENV_PREFIX}cms.$DNS_PUBLIC  ansible_ssh_host=$CMS

[central_tester]
${ENV_PREFIX}tester.$DNS_PUBLIC  ansible_ssh_host=$TESTER

[central_cmdb]
${ENV_PREFIX}cmdb.$DNS_PUBLIC  ansible_ssh_host=$CMDB

[central_postgres]
${ENV_PREFIX}postgres.$DNS_PUBLIC  ansible_ssh_host=$POSTGRES

[central_monitor]
${ENV_PREFIX}monitor.$DNS_PUBLIC  ansible_ssh_host=$MONITOR

#[central_hardening]
EOT
[[ "$CMS" == "disabled" ]] && sed -e '/^central_cms/ s/^/#/' \
  -e '/^\[central_cms/,+1 s/^/#/' -i $PROJECT_ROOT/$INVENTORY/$ENV
[[ "$POSTGRES" == "disabled" ]] && sed -e '/^central_postgres/ s/^/#/' \
  -e '/^\[central_postgres/,+1 s/^/#/' -i $PROJECT_ROOT/$INVENTORY/$ENV
[[ "$CMDB" == "disabled" ]] && sed -e '/^central_cmdb/ s/^/#/' \
  -e '/^\[central_cmdb/,+1 s/^/#/' -i $PROJECT_ROOT/$INVENTORY/$ENV
[[ "$MONITOR" == "disabled" ]] && sed -e '/^central_monitor/ s/^/#/' \
    -e '/^\[central_monitor/,+1 s/^/#/' -i $PROJECT_ROOT/$INVENTORY/$ENV
if [[ "$TESTER" == "disabled" ]]; then
  sed -e '/^central_tester/ s/^/#/' \
      -e '/^\[central_tester/,+1 s/^/#/' -i $PROJECT_ROOT/$INVENTORY/$ENV
  sed -e '/tester/ s/^/#/' -i $PROJECT_ROOT/Makefile
fi
}

setProxy() {
  printf "${GREEN}==> Setting ansible proxy${NORMAL}\n"
  if [[ ! -z "$proxy_host" ]]; then
cat << EOT > $ANSIBLE_VARS_FILE
---
proxy_host: $proxy_host
proxy_port: $proxy_port
proxy_login: $proxy_login
proxy_password: $proxy_pass
proxy_url: "$PROXY"
no_proxy: "localhost"
proxy_env:
  http_proxy: "{{proxy_url}}"
  https_proxy: "{{proxy_url}}"
  ftp_proxy: "{{proxy_url}}"
  no_proxy: "{{no_proxy}}"
  HTTP_PROXY: "{{proxy_url}}"
  HTTPS_PROXY: "{{proxy_url}}"
  FTP_PROXY: "{{proxy_url}}"
  NO_PROXY: "{{no_proxy}}"
EOT
  else
cat << EOT > $ANSIBLE_VARS_FILE
---
proxy_host:
proxy_port:
proxy_login:
proxy_password:
proxy_url: "http://{{proxy_login}}:{{proxy_password}}@{{proxy_host}}:{{proxy_port}}"
no_proxy: "localhost"
proxy_env:
  fake_param: fake_value
EOT
  fi
}

generateSSHKeys() {
  KEYFILE=$PROJECT_BAK/ansible/inventory/$PROJECT/.ssh/id-$PROJECT-ansible.rsa
  if [[ ! -f "$KEYFILE" ]]; then
    printf "${GREEN}==> Generating ssh keys${NORMAL}\n"
    bin/generate-ssh-keys.sh
  else
    printf "${GREEN}==> Using ${YELLOW}$KEYFILE ${NORMAL}\n"
    cp -a $PROJECT_BAK/ansible/inventory/$PROJECT/.ssh $PROJECT_ROOT/$INVENTORY/.ssh
  fi
}

deploySSHKeys(){
  printf "${GREEN}==> Deploying ansible keys${NORMAL}\n"
  bin/deploy-ansible-keys.sh
}

build() {
  printf "${GREEN}==> Building${NORMAL}\n"
  make build
}

bootstrap() {
  printf "${GREEN}==> Bootstraping${NORMAL}\n"
  make bootstrap
}

deploy() {
  set -e
  printf "${GREEN}==> Deploying $CODE_NAME ${NORMAL}\n"
  make deploy
}

postSetup() {
  printf "${GREEN}==> Post Setup${NORMAL}\n"
  set -x
  chown -hR jenkins:jenkins /etc/ansible
  chown jenkins:jenkins $PROJECT_ROOT -R
  chmod 750 /etc/ansible
  chown -R root:jenkins /var/lib/ansible
  chmod -R 770 /var/lib/ansible
  chmod 770 /var/lib/ansible/retries
  chown root:jenkins /var/log/ansible
  chmod 770 /var/log/ansible
  chown root:jenkins /var/log/ansible/ansible.log
  chmod 770 /var/log/ansible/ansible.log
  ln -s $PROJECT_ROOT/.env /var/lib/jenkins/.env
  chown -h jenkins:jenkins /var/lib/jenkins/.env*
  set +x
  systemctl restart jenkins
  printf "${GREEN}"
  echo '                                  '
  echo '   __ __                     _ _  '
  echo '  / //_/ ____  __ _  ___ _  (_|_) '
  echo ' /  <   /___/ /    \/ _ `/ / / /  '
  echo '/_/|_|       /_/_/_/\_,_/_/ /_/   '
  echo '                       |___/      '
  echo ''
  echo '  ____                        __       '
  echo ' / __ \_______ ___ _____ ___ / /____ _ '
  echo '/ /_/ / __/ _  / // / -_|_-</ __/ _ `/ '
  echo '\____/_/  \_  /\___/\__/___/\__/\_,_/  '
  echo '           /_/                         ....is now installed!'
  echo ''
  echo ''
  printf "${BLUE}==> Happy DevOps :) ${NORMAL}"
  echo ''
}

[[ "$#" -lt 2 ]] && usage
checkSystem
clone
[[ ! -f ".env" ]] && setEnv || source .env
checkEnvFile
createInventory
generateSSHKeys
build
bootstrap
deploySSHKeys
setProxy
deploy
setEnv
postSetup

exit 0
