#!/bin/bash

#
# script: deploy-ansible-keys.sh
# author: jorge.medina@kronops.com.mx
# desc: Copy ssh root keys to server
#

# Load all variables from .env and export them all for Ansible to read
set -o allexport
source .env
set +o allexport

ansible-playbook ansible/playbooks/copy-ssh-key-to-server.yml -k -K
