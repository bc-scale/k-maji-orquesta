#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class KVMHost(TestCase):
    def test_pkgs(s):
        packages = [
         'qemu-kvm','libvirt-bin',
         'virt-manager','virt-viewer','cpu-checker','virtinst','ssh-askpass',
         'e2fsprogs','xfsprogs','mdadm','lvm2','kpartx','lsscsi','nfs-common','open-iscsi','multipath-tools',
         'iproute','bridge-utils','vlan',
         'libsys-virt-perl','libxml-simple-perl','lzop']
        f.test_pkgs(s.host, packages, s.output)

    def test_file_exist(s):
        files = [
            '/kvm/images',
            '/var/lib/libvirt/vm-backups'
        ]
        f.test_file_exist(s.host, files, s.output)

    def test_pkgs(s):
        packages = ['qemu-utils']
        f.test_pkgs(s.host, packages, s.output)

    def test_services(s):
        f.test_service_running(s.host, ['libvirt-bin'], s.output)
        f.test_service_enabled(s.host, ['libvirt-bin'], s.output)
