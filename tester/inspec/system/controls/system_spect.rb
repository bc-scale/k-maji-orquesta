# encoding: utf-8

ip = command('hostname')
control ip.stdout  do
  title 'Check for services'
  describe service('cron') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe service('ssh') do
    puts ip.stdout
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe service('rsyslog') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe service('ntp') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe port(22) do
    it { should be_listening }
    its('processes') { should include 'sshd' }
    its('protocols') { should include 'tcp' }
    its('addresses') { should include '0.0.0.0' }
  end
end
