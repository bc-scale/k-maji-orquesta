# encoding: utf-8

ip = command('hostname')
control ip.stdout  do
  title 'Check for services'
  describe service('nagios') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe service('apache2') do
    puts ip.stdout
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
