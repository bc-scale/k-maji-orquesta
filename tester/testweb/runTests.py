#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
import os
import sys
from time import sleep as t

from termcolor import colored
from unittest import TextTestRunner, makeSuite, TestSuite
from xmlrunner import XMLTestRunner

from functions.functions import setWebDriver, resultTable, setOutputTable
from functions.functions import createScreenDirs
from functions.testLoader import getTest
from constants import constants as c

class RunTest(object):
    def __init__(self, browser):
        '''
        Set Up the webdriver and the output result table
        :param browser: Webdriver to use
        '''
        outHeaders = ['TEST', 'VARS', 'OUT']
        createScreenDirs()
        self.wasSuccessful = False
        self.output = setOutputTable(outHeaders)
        self.wd = setWebDriver(browser)

    def setUpTest(self, testClass):
        '''
        Create class objects
        :param testClass: test class definition
        '''
        testClass.wd = self.wd
        testClass.output = self.output
        return testClass

    def runTests(self, testName):
        '''
        Run a suit of tests
        :param testName: testClass name
        '''
        myTestCasesDict = getTest(testName)
        for testClass in myTestCasesDict['testCases']:
            test = self.setUpTest(testClass)
            ts = TestSuite(makeSuite(test, 'test'))
            print(colored(ts, 'grey'), file=sys.stderr)
            result = XMLTestRunner(output='%s' % c.xmlPathToSave).run(ts)
            self.wasSuccessful = True if result.wasSuccessful() else False
            print(self.wasSuccessful)

    def printOutput(self):
        '''
        Print the output result table
        '''
        resultTable(self.output)

    def quitDriver(self):
        '''
        Quit the webdriver session
        '''
        self.wd.quit()

if __name__ == '__main__':
    import traceback
    try:
        tests = os.getenv('tests', 'openurl').split(',')
        app = RunTest(os.getenv('browser', 'chrome'))
        for test in tests:
            app.runTests(test)
    except Exception as err:
        traceback.print_exc()
    finally:
            app.quitDriver()
            app.printOutput()
            if not app.wasSuccessful:
                assert False
                sys.exit(-1)
