# -*- mode: ruby -*-
# vi: set ft=ruby :

# Description: Vagrantfile for vagrant-virtualbox-deployer project.
# Author: Jorge Armando Medina
# Email: jorge.medina@kronops.com.mx

required_plugins = %w( vagrant-env vagrant-proxyconf)
required_plugins.each do |plugin|
  system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
end


Vagrant.configure("2") do |config|
  config.vm.define "deployer", primary: true do |deployer|
    config.env.enable # Enable vagrant-env(.env)
    config.proxy.http     = ENV['PROXY_URL']
    config.proxy.https    = ENV['PROXY_URL']
    deployer.vm.hostname = "deployer"
    deployer.vm.network "private_network", ip: "192.168.33.10"
    deployer.vm.network "forwarded_port", guest: 8080, host: 1088
    deployer.vm.synced_folder ".", "/vagrant", disabled: true
    deployer.vm.provision "fix-no-tty", type: "shell" do |s|
      s.privileged = false
      s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
    end
    deployer.ssh.username = "vagrant"
    deployer.ssh.password = "vagrant"
    deployer.ssh.insert_key = true
  end

  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--cpuexecutioncap", "30"]
    vb.memory = "512"
    vb.customize ["modifyvm", :id, "--cpus", "1"]
    vb.customize ["modifyvm", :id, "--ioapic", "on"]
  end
end
