redis-service
===============

Tasks for setting up redis server.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

Not at the moment.

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - redis-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
